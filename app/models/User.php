<?php
//Al 
require_once($_SERVER['DOCUMENT_ROOT'] . 'practica_home/core/Database.php');

Class User extends Database{//extender modelo jerarquico
    
    protected $_name;
    protected $_lastname;
    protected $_email;
    protected $_password;

    public function __construct(){
        //Llamamos al contructor del "padre" (Database), para que arranque
            parent::__construct();
    }

    public function getName(){
        return $this->_name;
    }

    public function setName($name){
        $this->_name = $name;
    }

    public function getLastname(){
        return $this->_lastname;
    }

    public function setLastname($lastname){
        $this->_lastname = $lastname;
    }

    public function getEmail(){
        return $this->_email;
    }

    public function setEmail($email){
        $this->_email = $email;
    }

    public function getPassword(){
        return $this->_password;
    }

    public function setPassword($password){
        $this->_password = $password;
    }

    public function indexUser(){
        // el * quiere decir que seleccionamos todo
        $query =  "SELECT *
        FROM `t_user`";
        
        $stmt = $this->_pdo->prepare($query);//propiedad del objeto database el cual esta extendiendo el modelo, en esta propiedad se esta creando la conxion entre php y la base de datos
        //preparando la consulta a la base de datos con query, stmt estamos almacenando la preparacion de la consulta para la base de datos
        $stmt->execute();//ejecuta la consulta
        $result = $stmt->fetchAll(); //result va a ser un array esta consulta conviertemelo en un array que sera un resultado que llegara al html

        return $result;
    }

    public function showUser($id){
        // el * quiere decir que seleccionamos todo
        $query =  "SELECT *
        FROM `t_user` 
        WHERE id = :id";//nombre del campo (columna)

        $stmt = $this->_pdo->prepare($query);
        $stmt->bindParam("id", $id);//el parametro id va a tener como valor el numero que le hemos pasado en la funcion
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        return $result;
    }

    public function createUser($user){

        $query = "insert into t_user (name, lastname, email, password) 
        values (:name, :lastname, :email, :password)";

        $stmt = $this->_pdo->prepare($query);
        $stmt->bindParam("name", $user['name']);
        $stmt->bindParam("lastname", $user['lastname']);
        $stmt->bindParam("email", $user['email']);
        $stmt->bindParam("password", $user['password']);
        $stmt->execute();

        $user_id = $this->_pdo->lastInsertId();

        return "Usuario añadido correctamente con el número de id " . $user_id;

    }

    public function updateUser($user){
        //mucha atencion!! despues de la ultima columna no se pone coma.
        $query = "UPDATE t_user
        SET `name` = :name, 
            `lastname` = :lastname, 
            `email` = :email  
        WHERE id = :id";

        $stmt = $this->_pdo->prepare($query);
        $stmt->bindParam("name", $user['name']);
        $stmt->bindParam("lastname", $user['lastname']);
        $stmt->bindParam("email", $user['email']);
        $stmt->bindParam("id", $user['id']);
        $stmt->execute();

        return "Usuario actualizado correctamente"; 
        
    }

    public function deleteUser($id){

        $query =  "DELETE 
        FROM `t_user` 
        WHERE id = :id";

        $stmt = $this->_pdo->prepare($query);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        
        return "Usuario eliminado correctamente con el número de id " . $id;
    }
}

?>