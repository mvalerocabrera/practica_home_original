<?php

class Blog {

    protected $_title;
    protected $_comment;
    protected $_category;
    protected $_date;

    public function __construct($blog){
        $this->_title = $blog['title'];
        $this->_comment = $blog['comment'];
        $this->_category = $blog['category'];
        $this->_date = date("d/m/Y");
    }

    public function showUser(){}

    public function createUser(){}

    public function updateUser(){}

    public function deleteUser(){}

}
?>