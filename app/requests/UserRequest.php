<?php

    require_once($_SERVER['DOCUMENT_ROOT'] . '/practica_home/app/controllers/UserController.php');
    require_once($_SERVER['DOCUMENT_ROOT'] . '/practica_home/core/Validation.php');

    $validation = new Validation();
    $validation->isEmpty($_POST['name'], 'name', 'nombre');
    $validation->isEmpty($_POST['lastname'], 'lastname', 'apellido');
    $validation->isEmpty($_POST['email'], 'email', 'email');
    $validation->minlength($_POST['name'], 2, 'name', 'nombre');
    $validation->minlength($_POST['lastname'], 5, 'lastname', 'apellido');
    $validation->minlength($_POST['email'], 5, 'email', 'email');
    $validation->maxlength($_POST['name'], 20, 'name', 'nombre');
    $validation->maxlength($_POST['lastname'], 20, 'lastname', 'apellido');
    $validation->maxlength($_POST['email'], 20, 'email', 'email');
    $validation->isEmail($_POST['email'], 'email');

    if ($validation->getValidation()){ 
        $user = new UserController(); //creo el objeto y lo meto en la variable user
        $response['_validation'] = $validation->getValidation();
        $response['message'] = $user->createUser($_POST);
        echo json_encode($response); //estamos utilizando el metodo getUserName si esta validado podremos ver el nombre y el apellido
    }else{
        echo json_encode($validation->getErrors()); //devolvemos a ajax (con echo)
    }
    
?>
