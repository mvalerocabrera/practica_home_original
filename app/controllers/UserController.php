<?php
//Llamamos al modelo correspondiente
require_once($_SERVER['DOCUMENT_ROOT'] . '/practica_home/app/models/User.php');

//Cargamos esta libreria que hemos descargado previamente, pq nuestra version de php es anterior
require_once($_SERVER['DOCUMENT_ROOT'] . '/practica_home/core/password.php');

//aqui se crea el objeto usuario
Class UserController {//crea el objeto usuario y devuelve el nombre y apellido del usuario
    
    protected $_user;

    public function __construct(){
        //Al llamar al modelo User, podemo susar
        $this->_user = new User();
    }

    public function indexUser(){

        return $this->_user->indexUser();

    }


    public function showUser($id){

        return $this->_user->showUser($id);

    }

    public function createUser($user){
        
        //con esta funcion predefinida encriptamos el password
        $user['password'] = password_hash($user['password'], PASSWORD_DEFAULT);
        return $this->_user->createUser($user);

    }

    public function updateUser($user){

        return $this->_user->updateUser($user);

    }

    public function deleteUser($id){

        return $this->_user->deleteUser($id);

    }
        
}

?>
