<body>
<!--Headboard-->
        <div class="container-fluid">
            <div class="header row">
                <img class="logo position-relative" src="img/logo.png" alt="Logotype">
                
                <div class="title media-body position-relative">
                    <h1>ALÓ≈MALLORCA</h1>
                </div>
                    
                <!--Menu NAVclass-->
                <nav class="menu-navbar navbar navbar-expand-lg navbar-light">
                    <a class="navbar-brand" href="#"></a>
                    
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="adminPanel.php">Panel</a>
                            </li>
                            
                            <li class="nav-item">
                                <a class="nav-link" href="formUser.php">Regístrate</a>
                            </li>
                            
                            <li class="nav-item">
                                <a class="nav-link" href="contacto.html">Contacto</a>
                            </li>
                            
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Panel de Control
                                </a>
                                
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">Usuario</a>
                                    <a class="dropdown-item" href="#">Mis sitios</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Cerrar sesión</a>
                                </div>
                            </li>
                        </ul>
                    </div>		
                </nav>
            </div>
        </div>