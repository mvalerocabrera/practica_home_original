//Evitar que cargue el CKEDITOR si no es necesario.
$(document).ready(function(){

    if($("textarea").length >0){
        CKEDITOR.replaceAll(function(textarea,config){
            if(textarea.className == "advance"){
                config.removeButtons = "About";
                config.uploadUrl = "";
                return true;
            }
        
        });   
    }

    /*if($("select").length > 0){
        $(".weapon-select2").select2();
    }*/
});


$(document).on("click", "#enviar", function(event){

    jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-záéíóúàèìòùäëïöüñ\s]+$/i.test(value);
    }, "Sólo se aceptan letras");  

    /*$("#contact-form").validate({

        errorClass:"invalid",
            
        rules: {
            name: {
                required: true,
                lettersonly: true,
                minlength: 3,
            },
            lastname: {
                required: true,
                lettersonly: true,
                minlength: 3,
            },
            email:{
                required: true,
                email: true,
                minlength: 3,
            },
            password:{
                required: true,
                minlength: 9,
            },
            mensajes:{
                required: function(){
                    CKEDITOR.instances.mensajes.updateElement();
                },
                minlength: 1,
                maxlenght: 450,
            }
        },

        messages: {
            name: {
                required: "El nombre es obligatorio",
                lettersonly: "Solo pueden introducir caracteres",
                minlength:"Debe escribir un minimo de 3 caracteres",

            },
            lastname: {
                required: "El apellido es obligatorio",
                lettersonly: "Solo pueden introducir caracteres",
                minlength:"Debe escribir un minimo de 3 caracteres",
            },
            email:{
                required: "Debe introducir un email valido",
            },
            password:{
                required: "El numero de telefono es incorrecto",
            },
            mensajes:{
                minlength: "Debes escribir un minimo de 1 caracteres",
                maxlenght: "No puedes escribir mas de 450 caracteres",
            },
        }
    });*/

//if($("#contact-form").valid()){

        event.preventDefault();
        //Variable para los datos del texto
        //var textdata = CKEDITOR.instances.editor1.getData();
        //Metemos todo el formulario dentro de un objeto
        var form = '#' + $('.admin-form').attr('id');
        var url = $(form).prop('action');
        var formData = new FormData($("#contact-form")[0]); //FormData va a convertir el objeto en json
        //Podemos añadirle más datos "manualmente" con append (como la de ckeditor)
        //el "editor1" (en este caso) machacará el previo que habrá generado el html.
        //formData.append("mensajes", textdata);

        //Checkeo para comprobar en la consola (F12) del navegador si se han metido bien todos los datos
        for(var pair of formData.entries()) {
            console.log(pair[0]+ ', '+ pair[1]);
        };  

        $.ajax({
            type: 'post',
            url: url,
            dataType: 'json',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {

                console.log(response);
                
                if(response._validation){
                    $(".success").text(response.message);
                    $("#contact-form").fadeOut();
                    $(".success-container").removeClass("hidden").fadeOut().fadeIn(); 
                }else{

                    /*Escribir los mensajes de error debajo de cada input*/
                    /*
                    $.each(response, function(i, item) {
                        if(item.message){
                            $("#" + item.id + "-error").text(item.message);
                        }
                    });*/

                    /* Escribir los errores todos juntos encima del formulario*/

                    $(".errors-container").removeClass("hidden");

                    $.each(response, function(i, item) {
                        if(item.message){
                            $(".errors").append("<li>" + item.message + "</li>");
                        }
                    });
                }
            },
    
            error: function(response){
                console.log(response);
            }
        });
    //};
    
    return false;
});

// $(document).on('click', '.modificar', function(e) { //esta (e) son los eventos que ocurren fuera del navegador para poder interactuar
    
//     //lo mismo que mas arriba pero cambiando clases e ids
//     var id = $(this).attr('id');
//     var url = $(".table").attr('name');
    
//     $.post( url, { id: id} ).done(function( response ) {
//         console.log(response);
//     });
    
// });

// $(document).on('click', '.eliminar', function(e) { //esta (e) son los eventos que ocurren fuera del navegador para poder interactuar
    
//     //lo mismo que mas arriba pero cambiando clases e ids
//     var id = $(this).attr('id');
//     var url = $('.table').attr('action');
    
//     //llamada ajax, envio el id del usurio a eliminar
//     $.post( url, { id: id} ).done(function( response ) {
//         console.log(response);
//     });

$(document).on('click', '.fila', function(e) { //esta (e, de evento) son los eventos que ocurren fuera del navegador para poder interactuar
    console.log(e.target);

    var id = $(e.target).attr('id'); //.target devuelve el elemento del DOM que disparó el evento (inicialmente)
    var url = 'app/controllers/';
    var formData = new FormData($(".admin-form")[0]);

    switch ($(e.target).attr('value')){
        
        case "Modificar":
            if($('input',this).attr("readonly")){
                $('input',this).removeAttr("readonly");
                $('input',this).removeClass("editar");
            }else{
                alert("No has guardado los cambios");
                $('input',this).prop('readonly', true);
                $('input',this).addClass("editar");
            }
            break;

        case "Guardar":
            $('input',this).prop('readonly', true);
            $('input',this).addClass("editar");
            //var guardar = new FormData($(`#${id}`)[0]);

            let name = $('input[name=name]',this).val();
            let lastname = $('input[name=lastname]',this).val();
            let email = $('input[name=email]',this).val();

            $.post( url + 'updateUser.php', { id: id, name: name, lastname: lastname, email: email } ).done(function( response ) {
            console.log(response);
            });
            break;
        
        case "Eliminar":
            if (confirm("¿Estas seguro de que deseas Eliminar el usuario con ID:"+id+"?")){
                //llamada ajax, envio el id del usurio a eliminar
                $.post( url + 'deleteUser.php', { id: id} ).done(function( response ) {
                console.log(response);
                }); 
                $(`#${id}`).addClass("hidden");//con `#${id}`estoy captando la id de la fila donde se dispara el evento--tambien podria poner (this)
                break;
            }   

        
    }
  

});