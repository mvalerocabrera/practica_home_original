<div class="col-xl-12 title-search">
    <h3>Formulario de Contacto</h3>
</div>

<div class="errors-container hidden">
    <ul class="errors"></ul>
</div>

<div class="success-container hidden">
    <h1 class="success"></h1>
</div>

<div class="container formulario">
     
    <form class="admin-form" id="FormSlider" action="SliderRequest.php" method="POST">

        <div class="form-group">
            <label for="title"><br>Titulo</label>
            <input type="text" class="form-control" id="title"  name="title" placeholder="Escriba su titulo">
            <p id="title-error" class="input-error"></p>
        </div>

        <div class="form-group">
            <label for="comment">Descripción</label>
            <textarea name="description" class="basic" id="description"></textarea>
        </div>

        <div class="form-group">
            <label for="link"><br>Enlace</label>
            <input type="text" class="form-control" id="link"  name="link" placeholder="Ponga su enlace">
            <p id="link-error" class="input-error"></p>
        </div>

        <div action="upload.php" method="post" enctype="multipart/form-data">
  
            <b>Enviar un archivo: </b>
            <br>
            <input name="image" type="file">

        </div>

        <div>
            <button id="enviar" class="btn btn-primary">Enviar</button>
        </div>
    </form>
    
 </div>

</div>