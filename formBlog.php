<div class="col-xl-12 title-search">
    <h3>Envia tu Formulario</h3>
</div>

<div class="errors-container hidden">
    <ul class="errors"></ul>
</div>

<div class="success-container hidden">
    <h1 class="success"></h1>
</div>

<div class="container formulario">
    
    <form class="admin-form" id="blogForm" action="app/requests/BlogRequest.php" method="POST" >

        <div class="form-group">
            <label for="title"><br>Titulo</label>
            <input type="text" class="form-control" id="title" name="title" placeholder="Escriba su titulo">
            <p id="title-error" class="input-error"></p>
        </div>

        <div class="form-group">
            <label for="skills">Categorias</label></br>
            <select class="select2" style="width:100%" name="category">
                <option value="Ciencia">Ciencia</option>
                <option value="Tecnologia">Tecnologia</option>
                <option value="Sociedad">Sociedad</option>
                <option value="Otros">Otros</option>
            </select>
        </div>

        <div class="form-group">
            <label for="comment">Comentario</label>
            <textarea name="comment" class="basic" id="comment" name"comment"></textarea>
        </div>

        <input type="submit" id="enviar" class="btn btn-primary" value="Enviar">
        
    </form>
    
</div>