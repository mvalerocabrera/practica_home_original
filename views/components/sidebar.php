<body> 
    <?php 
        //llama al objeto UserController para poder utilizar las funciones de ese objeto
        require_once "/App/Controllers/UserController.php"; 
        
        //creo la variable $user_obj para llamar al objeto UserController
        $user_obj = new UserController(); 
        $users = $user_obj->indexUser();
    ?>
    
    <div class="container col-xl-12">
    <div class="row">
        <div class="col-xl-2">
            <!-- As a heading -->
            <nav class="navbar navbar-light bg-light">
                <div><a class="col-xl-2 navbar-brand mb-0 h1 controlPanel" href="">Panel de Control</a></div>
                <div><a class="col-xl-2 navbar-brand mb-0 h1" href="">Usuarios</a></div>
                <div><a class="col-xl-2 navbar-brand mb-0 h1" href="">Blogs</a></div>
                <div><a class="col-xl-2 navbar-brand mb-0 h1" href="">Sliders</a></div>
            </nav>
        </div>