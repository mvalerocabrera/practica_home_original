        <div class="col-xl-10">
            <form id="userIndex" method="post" class="admin-form"> 
            <table id="table_sel" class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>NOMBRE</th>
                        <th>APELLIDOS</th>
                        <th>EMAIL</th>
                        <!--<th>PASSWORD</th>-->
                        <th>MODIFICAR</th>
                        <th>GUARDAR</th>
                        <th>ELIMINAR</th>
                    </tr>
                    <!--el foreach recorre todos los usuarios de la tabla a través de la variable $users arriba declarada
                    qu es =$user_obj =UserController->indexUser() y en el objerto UserController vemos que a través de la propiedad _user nos
                    lleva al objeto User a la función indexUser que lo que hay es un SELECT * que lo que hace es coger todos los datos 
                    de la tabla t_user y con la variable $user le decimos que vaya extrayendo la id,el name, el email...  este foreach es para que que envíe los datos de la base 
                    de datos a la tabla mediante la id, name....-->
                    <?php foreach($users as $user): ?> <!-- selecciona uno por uno del la lista users-->
                        <tr class="fila" id="<?= $user['id']; ?>">
                            <td ><?= $user['id']; ?></td>
                            <td>
                                <input 
                                    type = "text"
                                    class = "editar"
                                    name = "name"
                                    value = "<?= $user['name']; ?>" 
                                    onkeypress = "guardar(event, this)"
                                    readOnly
                                />
                            </td>
                            <td>
                                <input 
                                    type = "text"
                                    class = "editar" 
                                    name = "lastname"
                                    value = "<?= $user['lastname']; ?>" 
                                    onkeypress = "guardar(event, this)" 
                                    readOnly 
                                />
                            </td>
                            <td>
                                <input 
                                    type = "text"
                                    class = "editar" 
                                    name = "email"
                                    value = "<?= $user['email']; ?>" 
                                    onkeypress = "guardar(event, this)" 
                                    readOnly 
                                />
                            </td>
                            <!--<td><//?= $user['password']; ?></td>--> 

                            <!--Creamos botones en cada fila que tendra como ID el ID del usuario--> 
                            <td><button id=<?=$user['id']; ?> class="modificar input-text btn btn-outline-secondary btn-xs" type="button" name="modificar" value="Modificar"><i class="far fa-edit"></i></td>
                            <td><button id=<?=$user['id']; ?> class="modificar input-text btn btn-outline-info btn-xs" type="button" name="modificar" value="Guardar"><i class="far fa-save"></i></i></td>
                            <td><button id=<?=$user['id']; ?> class="eliminar input-text btn btn-outline-danger btn-xs" type="button" name="eliminar" value="Eliminar"><i class="fas fa-times fa-lg"></td>
                            <!-- Al tocar un boton, guardará su ID en este campo y se lo dara a Ajax para los eventos de "Delete" o "Show" o "Modify" -->
                        </tr> 
                    <?php endforeach ?>
                
                </thead>
            </table>
        </form> 

        <button id="newUser" class="btn btn-success btn-xs" type="button" name="newUser">+ Añadir usuario</button>
            </div>
        </div>
    </div>
</div>
    <div class="col-9 col-sm-9 col-md-6 col-lg-6 col-xl-9" id="contenido">
        

    </div>
     
</body>