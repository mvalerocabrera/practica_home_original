<?php

/*
|--------------------------------------------------------------------------
| Conexión a la base de datos mediante pdo
|--------------------------------------------------------------------------
|
| **Se recoge los parametros de configuración de la base de datos de configdb.php
| **Se activa MYSQL_ATTR_LOCAL_INFILE para permitir la importación de archivos
|    
*/ 

class Database { 
    
    private $_driver, $_host, $_database, $_user, $_password;

    public function __construct(){

        $db_cfg = require_once($_SERVER['DOCUMENT_ROOT'] . '/practica_home/config/database.php');

        $this->_driver = $db_cfg['driver'];
        $this->_host = $db_cfg['host'];
        $this->_database = $db_cfg['database'];
        $this->_user = $db_cfg['user'];
        $this->_password = $db_cfg['password'];

        try {

            $this->_pdo = new PDO($this->_driver .':host='.$this->_host.';dbname='.$this->_database, $this->_user, $this->_password, 
                array(
                    PDO::MYSQL_ATTR_LOCAL_INFILE => TRUE
            ));

            $this->_pdo->exec("SET CHARACTER SET utf8");

        } catch (PDOException $e) {

            echo "Error al conectar a la base de datos!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function  __destruct() {
        
        $this->_pdo = NULL;
    }
} 

?> 